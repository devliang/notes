package com.uber.notes;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioGroup;


public class SettingsActivity extends AppCompatActivity {
    private SharedPreferences mPref;

    public static final String BACKGROUND_COLOR_KEY = "BACKGROUND_COLOR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        mPref = PreferenceManager.getDefaultSharedPreferences(this);

        RadioGroup backgroundColorRadioGroup = (RadioGroup) findViewById(R.id.backgroundColorRadioGroup);
        int color = mPref.getInt(BACKGROUND_COLOR_KEY, Color.WHITE);
        if (color == Color.RED) {
            backgroundColorRadioGroup.check(R.id.backgroundColorRedRadio);
        } else if (color == Color.GREEN) {
            backgroundColorRadioGroup.check(R.id.backgroundColorGreenRadio);
        } else {
            backgroundColorRadioGroup.check(R.id.backgroundColorWhiteRadio);
        }

        backgroundColorRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int color;
                if (checkedId == R.id.backgroundColorRedRadio) {
                    color = Color.RED;
                } else if (checkedId == R.id.backgroundColorGreenRadio) {
                    color = Color.GREEN;
                } else {
                    color = Color.WHITE;
                }

                SharedPreferences.Editor editor = mPref.edit();
                editor.putInt(BACKGROUND_COLOR_KEY, color);
                editor.apply();
            }
        });
    }

    public void onSaveButtonClicked(View view) {
        finish();
    }

}
